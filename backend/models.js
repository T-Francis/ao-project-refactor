
const { playersDb, deathEventsDb, battlesDb, guildsDb } = require('./libs/db');
const Fetch = require('./libs/fetch');
const utils = require('./utils');

const Inventory = {
    hydrate(arg) {
        const inventory = [];
        for (const item of arg) {
            if (item !== null && item !== 'null')
                inventory.push({ type: item.type, quality: item.quality })
        }
        return inventory == [] ? inventory : null;
    }
};

const Gear = {
    hydrate(arg) {
        const o = () => {
            return { type: null, quality: null };
        };
        const gear = {
            mainHand: o(),
            offHand: o(),
            head: o(),
            armor: o(),
            shoes: o(),
            bag: o(),
            cape: o(),
            mount: o(),
            potion: o(),
            food: o(),
        };
        for (const key in gear) {
            if (arg.hasOwnProperty(key)) {
                for (const subKey in gear[key]) {
                    if (arg[key] !== null && arg[key].hasOwnProperty(subKey)) {
                        gear[key][subKey] = arg[key][subKey];
                    }
                }
            }
        }
        return gear;
    }
};

const Player = {
    hydrate(arg) {
        const playerData = utils.keysToCamel(arg);
        const player = {
            id: playerData.id,
            name: playerData.name,
            guildName: playerData.guildName,
            guildId: playerData.guildId,
            allianceName: playerData.allianceName,
            allianceId: playerData.allianceId,
            allianceTag: playerData.allianceTag,
            killFame: playerData.killFame,
            deathFame: playerData.deathFame,
            damage: playerData.damageDone,
            heal: playerData.supportHealingDone,
        };
        // equipment
        const equipment = playerData.equipment;
        if (equipment)
            player.gear = this.gear.hydrate(equipment);
        // inventory
        const inventory = playerData.inventory;
        if (inventory && inventory.length)
            player.inventory = this.inventory.hydrate(inventory);

        return player;
    },
    get({ id = null, name = null, eagerload = { kills: false, deaths: false } }) {
        const find = {};
        if (id !== null)
            find.id = id;
        if (name !== null)
            find.name = name;
        const result = playersDb.get('players').find(find).value();
        if (eagerload.kills)
            result.kills = DeathEvent.filter({ killerId: id });
        if (eagerload.deaths)
            result.deaths = DeathEvent.filter({ victimId: id });
        return result;
    },
    filter({ id = null, guildId = null, take = 10 }) {
        const filter = {};
        if (id !== null)
            filter.id = id;
        if (guildId !== null)
            filter.guildId = guildId;
        return playersDb.get('players').filter(filter).take(take).value();
    },
    sync(arg) {
        const player = this.hydrate(arg);
        const found = this.get({ id: player.id });
        // we dont care about some properties in basic player model
        delete player.gear;
        delete player.heal;
        delete player.inventory;
        delete player.kills;
        delete player.deaths;
        if (found) {
            // seem's the low-db assign method require that we also clear the found $ref
            delete found.gear;
            delete found.heal;
            delete found.inventory;
            delete found.kills;
            delete found.deaths;
            return playersDb.get('players').find({ id: player.id }).assign(player).write();
        } else {
            return playersDb.get('players').push(player).last().write();
        }
    },
    gear: Gear,
    inventory: Inventory,
};

const Guild = {
    hydrate(arg) {
        const guildData = utils.keysToCamel(arg);
        const guild = {
            id: guildData.id,
            name: guildData.name,
            founderId: guildData.founderId,
            founderName: guildData.founderName,
            founded: guildData.founded,
            tag: guildData.allianceTag,
            allianceId: guildData.allianceId,
            allianceName: guildData.allianceName,
            logo: guildData.logo,
            killFame: guildData.killFame,
            deathFame: guildData.deathFame,
            membersCount: guildData.memberCount,
            members: [],
            battles: []
        };
        return guild;
    },
    get({ id = null, allianceId = null, eagerload = { members: false, battles: false } }) {
        const find = {};
        if (id !== null)
            find.id = id;
        if (allianceId !== null)
            find.allianceId = allianceId;
        const guild = guildsDb.get('guilds').find(find).value();
        if (eagerload.members) {
            guild.members = Player.filter({ guildId: guild.id });
        }
        if (eagerload.battles) {
            guild.battles = Battle.filter({ guildId: guild.id });
        }
        return guild
    },
    sync(arg) {
        const guild = this.hydrate(arg);
        const found = this.get({ id: guild.id });
        delete guild.members;
        delete guild.battles;
        if (found) {
            delete found.members;
            delete found.battles;
            return guildsDb.get('guilds').find({ id: guild.id }).assign(guild).write();
        } else {
            return guildsDb.get('guilds').push(guild).last().write();
        }

    },
}

const DeathEvent = {
    hydrate(arg) {
        const deathEventData = utils.keysToCamel(arg);
        const deathEvent = {
            id: deathEventData.eventId,
            averageItemPower: deathEventData.victim.averageItemPower,
            totalKillFame: deathEventData.totalVictimKillFame,
            // SBI API is pretty fcked up as the battleId is a real battle only when it's different of the eventId
            battleId: deathEventData.battleId !== deathEventData.eventId ? deathEventData.battleId : null,
            timestamp: deathEventData.timeStamp,
            killer: Player.hydrate(deathEventData.killer),
            victim: Player.hydrate(deathEventData.victim),
            assists: this.assists.hydrate(deathEventData.participants),
        };
        return deathEvent;
    },
    get({ id = null }) {
        const find = {};
        if (id !== null)
            find.id = id;
        const deathEvent = deathEventsDb.get('deathEvents').find(find).value();
        return deathEvent;
    },
    filter({ id = null, victimId = null, killerId = null, battleId = null, take = 10 }) {
        const filter = {};
        if (id !== null)
            filter.id = id;
        if (victimId !== null)
            filter.victim = { id: victimId };
        if (killerId !== null)
            filter.killer = { id: killerId };
        if (battleId !== null)
            filter.battleId = battleId;
        return deathEventsDb.get('deathEvents').filter(filter).take(take).value();
    },
    sync(arg) {
        const deathEvent = this.hydrate(arg);
        return this.get({ id: deathEvent.id }) || deathEventsDb.get('deathEvents').push(deathEvent).last().write();
    },
    assists: {
        hydrate(participants) {
            const assists = [];
            for (const participant of participants) {
                assists.push(Player.hydrate(participant));
            }
            return assists;
        }
    }
};

const Battle = {
    hydrate(arg) {
        const battleData = utils.keysToCamel(arg);
        const battle = {
            id: battleData.id,
            startTime: battleData.startTime,
            endTime: battleData.endTime,
            timeout: battleData.timeout,
            totalFame: battleData.totalFame,
            totalKills: battleData.totalKills,
            players: [],
            guilds: [],
            alliances: [],
        }
        // players
        for (const id in battleData.players) {
            battle.players.push(
                utils.keysToCamel(battleData.players[id])
            );
        }
        // guilds
        for (const id in battleData.guilds) {
            battle.guilds.push(
                utils.keysToCamel(battleData.guilds[id])
            );
        }
        // alliances
        for (const id in battleData.alliances) {
            battle.alliances.push(
                utils.keysToCamel(battleData.alliances[id])
            );
        }
        return battle
    },
    get({ id = null, eagerload = false }) {
        const find = {};
        if (id !== null)
            find.id = id;
        const battle = battlesDb.get('battles').find(find).value();
        if (eagerload) {
            return battle
        } else {
            return battle
        }
    },
    filter({ id = null, guildId = null, allianceId = null, playerId = null, take = 10, eagerload = false }) {
        const filter = {};
        if (id !== null)
            filter.id = id;
        if (guildId !== null)
            filter.guilds = [{ id: guildId }];
        if (allianceId !== null)
            filter.alliances = [{ id: allianceId }];
        if (playerId !== null)
            filter.players = [{ id: playerId }];
        const battle = battlesDb.get('battles').filter(filter).take(take).value();
        if (eagerload) {
            return battle
        } else {
            return battle
        }
    },
    sync(arg) {
        const battle = this.hydrate(arg);
        const found = this.get({ id: battle.id });
        if (found) {
            return found;
        } else {
            //we sync the related deathEvents of the battle
            Fetch.battleEvents(battle.id).then(results => {
                for (const deathEvent of results) {
                    DeathEvent.sync(deathEvent);
                }
            })
            return this.save(battle);
        }
    },
    save(battle) {
        return battlesDb.get('battles').push(battle).last().write();
    },
};

module.exports = {
    inventory: Inventory,
    gear: Gear,
    player: Player,
    guild: Guild,
    deathEvent: DeathEvent,
    battle: Battle,
}