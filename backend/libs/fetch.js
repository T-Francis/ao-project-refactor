const fetch = require('node-fetch')

const baseUrl = `https://gameinfo.albiononline.com/api/gameinfo`;
const playersUrl = `${baseUrl}/players`;
const battlesUrl = `${baseUrl}/battles`;
const guildsUrl = `${baseUrl}/guilds`;
const guildBattlesUrl = `${battlesUrl}?limit=10&sort=recent&guildId`;
const eventsUrl = `${baseUrl}/events`;
const battleEventsUrl = `${eventsUrl}/?battleId`;

/**
 * Fetch wrapper
 */
module.exports = {
    async player(id) {
        const data = await this.exec(
            `${playersUrl}/${id}`
        );
        return data
    },
    async playerDeaths(id) {
        const data = await this.exec(
            `${playersUrl}/${id}/deaths`
        );
        return data
    },
    async battles(id) {
        const data = await this.exec(
            `${battlesUrl}/${id}`
        );
        return data
    },
    async guilds(id) {
        const data = await this.exec(
            `${guildsUrl}/${id}`
        );
        return data
    },
    async guildBattles(id) {
        const data = await this.exec(
            `${guildBattlesUrl}=${id}`
        );
        return data
    },
    async battleEvents(id) {
        const data = await this.exec(
            `${battleEventsUrl}=${id}`
        );
        return data
    },
    async exec(url, options = { timeout: 6000 }) {
        const data = await fetch(url, options)
            .then((res) => res.json())
            .then((json) => json)
            .catch((error) => {
                console.log(error);
            });
        return data;
    },
};