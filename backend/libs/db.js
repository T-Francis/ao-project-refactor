const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const playersAdapter = new FileSync('database/players.json')
const playersDb = low(playersAdapter)
playersDb.defaults({ players: [] })
    .write() // Set defaults

const deathEventsAdapter = new FileSync('database/events.json')
const deathEventsDb = low(deathEventsAdapter)
deathEventsDb.defaults({ deathEvents: [] })
    .write() // Set defaults 

const battlesAdapter = new FileSync('database/battles.json')
const battlesDb = low(battlesAdapter)
battlesDb.defaults({ battles: [] })
    .write() // Set defaults

const guildsAdapter = new FileSync('database/guilds.json')
const guildsDb = low(guildsAdapter)
guildsDb.defaults({ guilds: [] })
    .write() // Set defaults

module.exports = {
    playersDb: playersDb,
    deathEventsDb: deathEventsDb,
    battlesDb: battlesDb,
    guildsDb: guildsDb
}