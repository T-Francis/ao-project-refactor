
module.exports = {
    ucFirst(s) {
        return s.charAt(0).toUpperCase() + s.slice(1);
    },
    toCamel(s) {
        s = s.charAt(0).toLowerCase() + s.slice(1)
        return s.replace(/([-_][a-z])/ig, ($1) => {
            return $1.toUpperCase()
                .replace('-', '')
                .replace('_', '');
        });
    },
    isArray(a) {
        return Array.isArray(a);
    },
    isObject(o) {
        return o === Object(o) && !this.isArray(o) && typeof o !== 'function';
    },
    keysToCamel(o) {
        if (this.isObject(o)) {
            const n = {};

            Object.keys(o)
                .forEach((k) => {
                    n[this.toCamel(k)] = this.keysToCamel(o[k]);
                });

            return n;
        } else if (this.isArray(o)) {
            return o.map((i) => {
                return this.keysToCamel(i);
            });
        }
        return o;
    }
}