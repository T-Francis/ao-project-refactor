const express = require('express');

const app = express();
const api = require('./routes/api');
const legacy = require('./routes/legacy');

app.use('/', legacy);
app.use('/api', api);

app.listen(process.env.PORT || 8000);
