
const express = require('express');
const router = express.Router();
const cors = require('cors');
const fetch = require('./../libs/fetch');

const { player, deathEvent, battle, guild } = require('../models');

/**
 * API endpoint fetch the offical API then return a synchronised and business data focused version from our DB
 * NOTE: all the `sync` methods will be moved into a future module that act like crontabs 
 */

// fetch player data from official api then sync & return the fresh db version
router.get('/players/:id/', cors(), async (req, res) => {
    const result = await fetch.player(req.params.id);
    player.sync(result);
    res.json(
        player.get({ id: req.params.id, eagerload: { kills: true, deaths: true } })
    );
});

router.get('/players/:id/deaths', cors(), async (req, res) => {
    const result = await fetch.playerDeaths(req.params.id);
    const deaths = [];
    for (const eventData of result) {
        deaths.push(
            deathEvent.sync(eventData)
        );
    }
    res.json(deaths);
});

router.get('/guilds/:id', cors(), async (req, res) => {
    const result = await fetch.guilds(req.params.id);
    guild.sync(result)
    res.json(
        guild.get({ id: req.params.id, eagerload: { members: true, battles: true } })
    );
});

router.get('/guilds/:id/battles', cors(), async (req, res) => {
    const result = await fetch.guildBattles(req.params.id);
    const battles = [];
    for (const battleData of result) {
        battles.push(
            battle.sync(battleData)
        );
    }
    res.json(battles);
});

module.exports = router;

